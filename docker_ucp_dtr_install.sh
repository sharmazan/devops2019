#!/bin/bash

# Install UCP
# install UCP on Network interface in Private network = 192.168.200.102
docker container run --rm --name ucp \
  -v /var/run/docker.sock:/var/run/docker.sock \
  docker/ucp:3.2.1 install \
  --host-address 192.168.200.102 \
  --pod-cidr "192.168.0.0/18" \
  --admin-username admin \
  --admin-password adminadmin \
  --license "$(cat /vagrant/docker_subscription.lic)" \
  --force-minimums \
  && echo "UCP installed successfully. Login to https://192.168.200.102 with login admin pass adminadmin"
  
# Login to https://192.168.200.102 or to https://127.0.0.1:8443/login/
# UCP login admin
# password adminadmin  


# Install DTR
docker container run --rm \
  docker/dtr:2.7.2 install \
  --ucp-node swarmmaster \
  --ucp-insecure-tls \
  --ucp-url https://192.168.200.102:443 \
  --replica-https-port 4443 \
  --replica-http-port 880 \
  --ucp-username admin \
  --ucp-password adminadmin && echo "DTR installed successfully! Login to https://192.168.200.102:4443"
